import { SET_PRODUCT_LIST_DATA } from '../mutation-types';

/**
 * @typedef {Object} ProductModel
 * @property {Number} id - an ID
 * @property {String} name - name product
 * @property {Number} price - price product
 * @property {String} img - img product
 */

/**
 * @typedef {Object} state product
 * @property {ProductModel[]} list - list of product
 */
const state = {
        list: []
    },

    mutations = {
        /**
         * Set product list data
         * @param state
         * @param {ProductModel[]} data
         */
        [SET_PRODUCT_LIST_DATA] (state, data) {
            state.list = data;
        }
    },

    getters = {
        /**
         * get all products
         * @param state
         * @returns {ProductModel[]}
         */
        getProductList (state) {
            return state.list;
        }
    };

export default {
    state,
    mutations,
    getters
};
