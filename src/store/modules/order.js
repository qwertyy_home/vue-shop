import { ADD_ORDER, REMOVE_ORDER, REMOVE_ALL_ORDER, UPDATE_COUNT_ORDER, LOAD_ORDERS_LIST } from '../mutation-types';

/**
 * @typedef {Object} EntityOrderModel
 * @property {Number} productId - product id
 * @property {Number} count - count products
 */

/**
 * @typedef {EntityOrderModel} OrderModel
 * @property {Number} id - an ID
 * @property {Boolean} enabled
 * @property {Number} date - date order
 */

/**
 * @typedef {Object} state order
 * @property {OrderModel[]} list - list of orders
 * @property {Number} counterId - order counter for id
 */
const state = {
        list: [],
        counterId: 0
    },

    mutations = {
        /**
         * Set order list data
         * @param state
         * @param {OrderModel[]} data
         */
        [LOAD_ORDERS_LIST]: function (state, data) {
            state.list = data;
        },

        /**
         * Add order
         * @param state
         * @param {EntityOrderModel} data
         */
        [ADD_ORDER]: function (state, data) {
            const order = {
                id: Date.now() + state.counterId,
                enabled: true,
                date: Date.now(),
                productId: data.productId,
                count: Number.parseInt(data.count)
            };

            state.counterId++;

            state.list.push(order);
        },

        /**
         * Update count product for order
         * @param state
         * @param {Object} data
         * @param {Number} data.orderId - order id
         * @param {Number} data.count - count product
         */
        [UPDATE_COUNT_ORDER]: function (state, data) {
            const orderItem = getOrder(data.orderId);

            if (orderItem) {
                orderItem.count = data.count;
            }
        },

        /**
         * Remove order
         * @param state
         * @param {Number} orderId - order id
         */
        [REMOVE_ORDER] (state, orderId) {
            const orderItem = getOrder(orderId);

            if (orderItem) {
                orderItem.enabled = false;
            }
        },

        /**
         * Remove all order
         * @param state
         */
        [REMOVE_ALL_ORDER] (state) {
            state.list = [];
        }
    },

    getters = {
        /**
         * get all order list
         * @param state
         * @returns {OrderModel[]}
         */
        getOrderList (state) {
            return state.list;
        },

        /**
         * get only enabled order list
         * @param state
         * @returns {OrderModel[]}
         */
        getOrderEnabledList (state) {
            return state.list.filter((item) => {
                return item.enabled;
            });
        }
    };

/**
 * find order by id
 * @param {Number} id: order id
 * @returns {*}
 */
function getOrder (id) {
    return state.list.find(function (item) {
        return item.id === id;
    });
}

export default {
    state,
    mutations,
    getters
};
