import store from 'store';
import { SET_PRODUCT_LIST_DATA } from './mutation-types';

/**
 * Load fake data to store
 */
export default function loadData () {
    /**
     * @type {ProductModel[]}
     */
    const data = [{
        name: 'name #1',
        price: 1250,
        img: 'https://picsum.photos/330/150/?image=357',
        id: 1
    }, {
        name: 'name #2',
        price: 100,
        img: 'https://picsum.photos/330/150/?image=15',
        id: 2
    }, {
        name: 'name #3',
        price: 2500,
        img: 'https://picsum.photos/330/150/?image=250',
        id: 3
    }, {
        name: 'name #4',
        price: 125,
        img: 'https://picsum.photos/330/150/?image=260',
        id: 4
    }, {
        name: 'name #5',
        price: 3600,
        img: 'https://picsum.photos/330/150/?image=17',
        id: 11
    }, {
        name: 'name #6',
        price: 1500,
        img: 'https://picsum.photos/330/150/?image=24',
        id: 21
    }, {
        name: 'name #7',
        price: 400,
        img: 'https://picsum.photos/330/150/?image=16',
        id: 31
    }, {
        name: 'name #8',
        price: 200,
        img: 'https://picsum.photos/330/150/?image=36',
        id: 41
    }];

    store.commit(SET_PRODUCT_LIST_DATA, data);
}
