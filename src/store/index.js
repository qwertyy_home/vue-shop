import Vue from 'vue';
import Vuex from 'vuex';

import products from './modules/products';
import order from './modules/order';

Vue.use(Vuex);

const options = {
    modules: {
        products,
        order
    }
};

export default new Vuex.Store(options);
