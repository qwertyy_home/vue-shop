import store from 'store';
import { LOAD_ORDERS_LIST } from './mutation-types';

/**
 * Save orders data(from store(Vuex)) to localStorage
 */
function saveOrderStoreToStorage () {
    const storageOrderData = store.getters.getOrderEnabledList;

    localStorage.setItem('orderStoreList__storage', JSON.stringify(storageOrderData));
}

/**
 * Add event for save orders data(from store(Vuex)) to localStorage
 */
export function initSaveToStorageOrder () {
    window.addEventListener('beforeunload', function () {
        saveOrderStoreToStorage();
    });
}

/**
 * Load data orders from localStorage to store(Vuex)
 */
export function loadOrderStoreFromStorage () {
    const storageOrderData = JSON.parse(localStorage.getItem('orderStoreList__storage'));

    if (storageOrderData) {
        store.commit(LOAD_ORDERS_LIST, storageOrderData);
    }
}
