/**
 * Adding zero to two characters
 * @param {Number} val
 * @returns {string}
 */
function padLeftZero (val) {
    const result = val.toString().length > 1 ? val : `0${val}`;

    return result;
}

/**
 * Format date to dd.MM.yyyy hh:mm:ss
 * @param {Number} date (number of milliseconds elapsed since January 1, 1970 00:00:00)
 * @returns {string}
 */
export default function formatDate (date) {
    const dateObj = new Date(date),
        month = padLeftZero((dateObj.getMonth() + 1)),
        day = padLeftZero(dateObj.getDate()),
        year = padLeftZero(dateObj.getFullYear()),
        hours = padLeftZero(dateObj.getHours()),
        minutes = padLeftZero(dateObj.getMinutes()),
        seconds = padLeftZero(dateObj.getSeconds()),
        result = `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;

    return result;
}
