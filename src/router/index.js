import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/**
 * async load component
 * @param {String} pageName : name page component
 * @returns {function(): *}
 */
function loadPage (pageName) {
    return () => import(`components/pages/${pageName}`);
}

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            meta: {
                title: 'home'
            },
            redirect: 'product-list'
        },
        {
            path: '/product-list',
            name: 'ProductList',
            meta: {
                title: 'Product List'
            },
            component: loadPage('ProductListPage')
        },
        {
            path: '/order',
            name: 'Order',
            meta: {
                title: 'Order'
            },
            component: loadPage('OrderPage')
        }
    ]
});

/**
 * Updating title for changes route
 */
router.beforeEach((to, from, next) => {
    document.title = to.meta ? to.meta.title : 'VUE - shop';

    next();
});

export default router;
